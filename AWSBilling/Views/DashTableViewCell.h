//
//  DashTableViewCell.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 11/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashTableViewCell : UITableViewCell

{
    IBOutlet UILabel *productLabel;
    IBOutlet UILabel *amountLabel;
}

-(void)populateCell:(NSString*)product withAmount:(NSString*)amount;

@end
