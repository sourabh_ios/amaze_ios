//
//  DashTableViewCell.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 11/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import "DashTableViewCell.h"

@implementation DashTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)populateCell:(NSString*)product withAmount:(NSString*)amount {
    productLabel.text = product;
    amountLabel.text = amount;
}

@end
