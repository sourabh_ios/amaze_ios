//
//  DashCollectionViewCell.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 11/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import "DashCollectionViewCell.h"

@implementation DashCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(10.0, 75.0)];
    [path addLineToPoint:CGPointMake(240.0, 75.0)];
  //  Create a CAShapeLayer that uses that UIBezierPath:
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[self colorFromHexString:@"#f15a29"] CGColor];
    shapeLayer.lineWidth = 2.0;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
  //  Add that CAShapeLayer to your view's layer:
    
    [self.layer addSublayer:shapeLayer];
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

-(void)populateCell:(NSString*)accountName withAmount:(NSString*)amount {
    accountIDLabel.text = accountName;
    amountLabel.text = amount;
}
@end
