//
//  UsageDetailCell.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 15/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import "UsageDetailCell.h"

@implementation UsageDetailCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)populateCell:(NSString*)userType withDescription:(NSString*)descValue Quantity: (NSString *)quantity andCost: (NSString *)cost {
    [self.itemDescValueLabel sizeToFit];
    self.userTypeValueLabel.text = userType;
    self.itemDescValueLabel.text = descValue;
    self.usageQuantityValueLabel.text = quantity;
    self.totalCostValueLabel.text = cost;
}
@end
