//
//  ConfigureStepView.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 16/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import "ConfigureStepView.h"
#import "RecieveBillView.h"

@implementation ConfigureStepView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

static ConfigureStepView *sharedInstance = nil;

+ (ConfigureStepView *)sharedInstance {
    @synchronized(self) {
        if (sharedInstance == nil) {
            //sharedInstance = [[[NSBundle mainBundle] loadNibNamed:@"ConfigureStepView" owner:self options:nil] objectAtIndex:0];
        }
    }
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;
        }
    }
    return nil;
}

- (void) open {
    self.hidden = FALSE;
    self.bucketTextField.text = self.bucketNameString;
    self.bucketCopyButton.layer.cornerRadius = 13;
    UIColor *themeColor = [self colorFromHexString:@"#f15a29"];
    self.bucketTextField.layer.borderWidth=1.1;
    self.bucketTextField.layer.cornerRadius = 3;
    self.bucketTextField.layer.borderColor = [themeColor CGColor];
}

- (IBAction)removeView:(id)sender {
    [self removeFromSuperview];
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
- (IBAction)Copy_Clicked:(id)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.bucketNameString;
    [self.bucketCopyButton setTitle:@"Copied" forState:UIControlStateNormal];
    self.bucketCopyButton.enabled = NO;
}

- (IBAction)Next_Clicked:(id)sender {
    [self removeView:0];
    [[RecieveBillView sharedInstance] open];
}
@end
