//
//  DashCollectionViewCell.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 11/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashCollectionViewCell : UICollectionViewCell
{
    IBOutlet UILabel *accountIDTextLabel;
    IBOutlet UILabel *accountIDLabel;
    IBOutlet UILabel *amountLabel;
}

-(void)populateCell:(NSString*)accountName withAmount:(NSString*)amount;
@end
