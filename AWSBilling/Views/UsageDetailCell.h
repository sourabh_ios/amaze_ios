//
//  UsageDetailCell.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 15/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsageDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *userTypeValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemDescValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *usageQuantityValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *usageQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCostLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCostValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *usageTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemDescLabel;


-(void)populateCell:(NSString*)userType withDescription:(NSString*)descValue Quantity: (NSString *)quantity andCost: (NSString *)cost;

@end
