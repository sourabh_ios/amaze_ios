//
//  RightPanel.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 24/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import "RightPanel.h"

@implementation RightPanel

static RightPanel *sharedInstance = nil;

+ (RightPanel *)sharedInstance {
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[[NSBundle mainBundle] loadNibNamed:@"RightPanel" owner:self options:nil] objectAtIndex:0];
        }
    }
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;
        }
    }
    return nil;
}

-(void)awakeFromNib
{
}

//** Animated opening of left menu screen **//
-(void)open
{
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
    buttonContainer.layer.cornerRadius = 3;
    buttonContainer.frame=CGRectMake(self.frame.size.width, buttonContainer.frame.origin.y, buttonContainer.frame.size.width, buttonContainer.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.3];
    
    buttonContainer.frame=CGRectMake(self.frame.size.width-buttonContainer.frame.size.width, buttonContainer.frame.origin.y, buttonContainer.frame.size.width, buttonContainer.frame.size.height);
    
    [UIView commitAnimations];
}

//** Function removes the leftmenu view from home screen **//
-(void)removeNow
{
    [self removeFromSuperview];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

//** Animated closing of left menu screen **//
-(void)close
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.3];
    buttonContainer.frame=CGRectMake(self.frame.size.width, buttonContainer.frame.origin.y, buttonContainer.frame.size.width, buttonContainer.frame.size.height);
    
    [UIView commitAnimations];
}

//** Animated closing of left menu screen when user taps anywhere in the right side of screen **//
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self close];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RightPanelDismissed" object:nil];
    [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(removeNow) userInfo:nil repeats:NO];
}
- (IBAction)Bucket_Changed:(id)sender {
    [self close];
    [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(removeNow) userInfo:nil repeats:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RightPanelDismissed" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BucketChangedTapped" object:nil];
}

- (IBAction)LogOut:(id)sender {
    [self close];
    [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(removeNow) userInfo:nil repeats:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RightPanelDismissed" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LogOutTapped" object:nil];
}


@end
