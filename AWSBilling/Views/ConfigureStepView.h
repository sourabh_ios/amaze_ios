//
//  ConfigureStepView.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 16/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfigureStepView : UIView
@property (weak, nonatomic) IBOutlet UITextField *bucketTextField;
@property (weak, nonatomic) IBOutlet UIButton *bucketCopyButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (nonatomic, strong) NSString *bucketNameString;

+ (ConfigureStepView *)sharedInstance;
- (void)open;
@end
