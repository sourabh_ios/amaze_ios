//
//  RightPanel.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 24/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightPanel : UIView
{
    IBOutlet UIButton *bucketChangeButton;
    IBOutlet UIButton *logOutButton;
    IBOutlet UIView *buttonContainer;
    IBOutlet UIView *settingsView;
}

+ (RightPanel *)sharedInstance;
-(void)open;

@end
