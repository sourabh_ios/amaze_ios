//
//  UsageFirstRowCell.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 22/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import "UsageFirstRowCell.h"

@implementation UsageFirstRowCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)populateCell:(NSString*)invoiceDate withPayer:(NSString*)payer BillingPeriod: (NSString *)period andAmount: (NSString *)amount {
    self.dateValueLabel.text = invoiceDate;
    self.payerName.text = payer;
    self.billingPeriod.text = period;
    self.amountValue.text = amount;
}
@end
