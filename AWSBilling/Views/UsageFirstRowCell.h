//
//  UsageFirstRowCell.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 22/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsageFirstRowCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *payerName;
@property (weak, nonatomic) IBOutlet UILabel *billingPeriod;
@property (weak, nonatomic) IBOutlet UILabel *amountValue;


-(void)populateCell:(NSString*)invoiceDate withPayer:(NSString*)payer BillingPeriod: (NSString *)period andAmount: (NSString *)amount;

@end
