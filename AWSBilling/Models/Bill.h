//
//  Usage.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 05/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bill: NSObject

@property (nonatomic, copy) NSString *monthTotal, *monthName;
@property (nonatomic, strong) NSArray *monthDetails;
@end
