//
//  Product.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 05/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic, copy) NSString *productName, *productAmount, *linkedAccount, *linkID, *usageType, *usageTotal, *usageDescription, *currency;
@property (nonatomic, copy) NSString *invoiceDate, *productPayer, *productSeller, *productAddress, *productQuantity, *abbrevatedName, *imageName;
@property (nonatomic) float productPrice;
@property (nonatomic, strong) NSMutableArray *productDetails;
@property (nonatomic, copy) NSString *billingPeriodStart, *billingPeriodEnd;

@end
