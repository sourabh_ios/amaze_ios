//
//  Users.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 12/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Users : NSObject

@property (nonatomic, copy) NSString *userName, *userID, *userAmount;
@property (nonatomic, strong) NSMutableArray *userProductList;

@end
