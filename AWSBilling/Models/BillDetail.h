//
//  BillDetail.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 12/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BillDetail : NSObject

@property (nonatomic, copy) NSString *payerName, *payerID, *payerAmount, *monthName;
@property (nonatomic, strong) NSArray *productsNameArray;
@property (nonatomic, strong) NSDictionary *productsDetailsDictionary;
@end
