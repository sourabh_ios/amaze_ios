//
//  UsageDetailViewController.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 15/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import "UsageDetailViewController.h"
#import "UsageDetailCell.h"
#import "UsageFirstRowCell.h"
#import "Product.h"
#import "RightPanel.h"

@interface UsageDetailViewController ()

@end

@implementation UsageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.filteredList = [[NSMutableArray alloc] init];

    // Do any additional setup after loading the view.
    if (self.productList.count > 0) {
        Product *product = [self.productList objectAtIndex:0];
        self.navigationItem.title = product.abbrevatedName;
    }
    else
        self.navigationItem.title = @"Detailed Billing";
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    //    self.productNameLabel.text = product.productName;
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc]
                                initWithTitle:@"Back"
                                style:UIBarButtonItemStyleBordered
                                target:self
                                action:nil];
    self.navigationController.navigationBar.topItem.backBarButtonItem=btnBack;
    isFiltered = false;
    
    self.listTableView.contentInset = UIEdgeInsetsMake(-63, 0, 0, 0);

    self.searchBar.barTintColor = [UIColor whiteColor];
    self.searchBar.layer.borderWidth=1.1;
    self.searchBar.layer.borderColor = [[self colorFromHexString:@"#f15a29"] CGColor];
    self.searchBar.returnKeyType = UIReturnKeyDone;
    self.searchBar.enablesReturnKeyAutomatically = NO;
    self.searchBar.delegate = self;
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    UIImage* image4 = [UIImage imageNamed:@"3Dots.png"];
    CGRect frameimg1 = CGRectMake(0, 0, image4.size.width, image4.size.height);
    UIButton *someButton1 = [[UIButton alloc] initWithFrame:frameimg1];
    [someButton1 setBackgroundImage:image4 forState:UIControlStateNormal];
    [someButton1 addTarget:self action:@selector(rightPanelButton:)
          forControlEvents:UIControlEventTouchUpInside];
    [someButton1 setShowsTouchWhenHighlighted:YES];
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:someButton1];
    self.navigationItem.rightBarButtonItem=mailbutton1;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rightPanelDismissed) name:@"RightPanelDismissed" object:nil];

}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

-(void)rightPanelDismissed
{
    self.view.alpha = 1.0;
    self.navigationController.navigationBar.alpha = 1.0;
}
- (IBAction)rightPanelButton:(id)sender
{
    [[RightPanel sharedInstance] open];
    self.view.alpha = 0.5;
    self.navigationController.navigationBar.alpha = 0.5;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Table View Data Source

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0)
        return 83;
    else
        return 103;
}

- (void)tableView: (UITableView*)tableView willDisplayCell: (UITableViewCell*)cell forRowAtIndexPath: (NSIndexPath*)indexPath
{
    if (indexPath.row == 0) {
    }
    else if(indexPath.row % 2 == 0)
        cell.backgroundColor = [UIColor lightGrayColor];
    else
        cell.backgroundColor = [UIColor whiteColor];
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if (isFiltered)
        return self.filteredList.count + 1;
    else
        return self.productList.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"UsageDetailCell";
    static NSString *CellIdentifier1 = @"UsageFirstCell";
     Product *product;
    if (indexPath.row == 0) {
        UsageFirstRowCell *detailTableCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1 forIndexPath:indexPath];
        product = [self.productList objectAtIndex:indexPath.row];
        NSString *billingPeriod = [NSString stringWithFormat:@"%@-%@", product.billingPeriodStart, product.billingPeriodEnd];
        [detailTableCell populateCell:product.invoiceDate withPayer:product.productPayer BillingPeriod:billingPeriod andAmount:self.totalAmount];
        return detailTableCell;
    }
   
    else
    {
        UsageDetailCell *detailTableCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        if (isFiltered)
            product = [self.filteredList objectAtIndex:indexPath.row-1];
        else
            product = [self.productList objectAtIndex:indexPath.row-1];
        
        NSString *tempString = [NSString stringWithFormat:@"%.2f",product.productPrice];
        [detailTableCell populateCell:product.usageType withDescription:product.usageDescription Quantity:product.productQuantity andCost:tempString];
        return detailTableCell;
    }
    
}


- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    [self.searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    isFiltered = true;
    [self.filteredList removeAllObjects];
    for (Product *product in self.productList) {
        NSString *titleCompare = product.usageType;
        NSString *descriptionCompare = product.usageDescription;
        NSRange rang = [titleCompare rangeOfString:searchText options:NSCaseInsensitiveSearch];
        NSRange rang1 = [descriptionCompare rangeOfString:searchText options:NSCaseInsensitiveSearch];
        if (rang.length == [searchText length] || rang1.length == [searchText length] ) {
            [self.filteredList addObject:product];
        }
    }
    [self.listTableView reloadData];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self.listTableView removeGestureRecognizer:tap];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self.listTableView addGestureRecognizer:tap];
}


@end
