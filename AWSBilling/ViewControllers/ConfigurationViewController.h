//
//  ConfigurationViewController.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 16/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConfigureStepView.h"
#import "RecieveBillView.h"

@interface ConfigurationViewController : UIViewController {
    NSString *errorString;
    NSString *ownerName;
}
@property (weak, nonatomic) IBOutlet UIImageView *cloudRotImageView;
@property (weak, nonatomic) IBOutlet ConfigureStepView *configureStepView;
@property (weak, nonatomic) IBOutlet RecieveBillView *receiveBillView;
@property (weak, nonatomic) IBOutlet UIButton *receiveButton;
@property (weak, nonatomic) IBOutlet UIButton *createButton;
@end
