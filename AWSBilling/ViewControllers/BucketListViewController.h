//
//  BucketListViewController.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 03/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BucketListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *bucketNames;
    NSMutableArray *objects;
    
    int parserLine;
    NSString *payerName;
    NSString *payerAccountID;
    NSString *payerAddress;
    NSMutableArray *productList;
    NSMutableArray *productSubList;
    BOOL endOfProductList;
    int billParseEndState;
    BOOL reverseArray;
    BOOL bucketFound;
    BOOL stayPage;
    
}
@property (nonatomic, strong) NSMutableArray *dashBoardList;
@property (weak, nonatomic) IBOutlet UITableView *bucketTableView;
@property (nonatomic, strong) NSArray *bucketList;
@property (nonatomic, strong) NSMutableArray *linkAccounts;
@property (nonatomic, weak) IBOutlet UINavigationBar *naviBar;
@end
