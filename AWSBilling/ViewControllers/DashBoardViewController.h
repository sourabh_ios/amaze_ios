//
//  DashBoardViewController.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 10/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"

@interface DashBoardViewController : UIViewController <XYPieChartDataSource, XYPieChartDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegate>
{
    NSMutableArray *bucketFiles;
    NSMutableArray *productCSVList;
    BOOL endOfProductList;
    int billParseEndState;
    int parserLine;
    NSString *productAmountString;
    NSString *accountNameString;
    
    NSString *selectedMonth;
}

@property (nonatomic) BOOL autoRefresh;
@property (nonatomic, strong) NSMutableArray *dashBoardList;
@property (strong, nonatomic) IBOutlet   XYPieChart *pieChart;
@property(nonatomic, strong) NSMutableArray *slices;
@property(nonatomic, strong) NSArray        *sliceColors;
@property (weak, nonatomic) IBOutlet UIScrollView *dashScrollView;
@property (weak, nonatomic) IBOutlet UILabel *circleLabel;
@property (weak, nonatomic) IBOutlet UITableView *productListTable;
@property (weak, nonatomic) IBOutlet UICollectionView *dashCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *leftArrow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *topProductsLabel;

@property (nonatomic, retain) NSArray *csvData;
@property (weak, nonatomic) IBOutlet UIButton *rightArrow;
@property (nonatomic, strong) NSMutableArray *productList;
@property (nonatomic, strong) NSMutableArray *monthList;
@property (nonatomic, strong) NSString *payerName;
@property (nonatomic, strong) NSString *payerID;

@property (nonatomic, retain) NSArray *productDetailsList;
@property (nonatomic, strong) NSArray *linkAccounts;
@property (nonatomic, strong) NSMutableArray *linkCSVAccounts;

@property (nonatomic, strong) UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;

@end
