//
//  ConfigurationViewController.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 16/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import "ConfigurationViewController.h"
#import "AmazonClientManager.h"
#import "MBProgressHUD.h"
#import "ConfigureStepView.h"
#import "RecieveBillView.h"
#import "JNKeyChain.h"
#import "Constants.h"

@interface ConfigurationViewController () <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}
@end

@implementation ConfigurationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = FALSE;
    self.navigationItem.hidesBackButton = TRUE;
    self.navigationItem.title = @"Configuration";
    self.navigationController.navigationBar.barTintColor = [self colorFromHexString:@"#f15a29"];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.cloudRotImageView.transform = CGAffineTransformMakeRotation(0.92502450356);
    //[self.view addSubview:RecieveBillView]
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(configurationDismissed) name:@"ConfigurationDismissed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recieveBillDismissed) name:@"RecieveBillDismissed" object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) drawLine {
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(10.0, 230.0)];
    [path addLineToPoint:CGPointMake(self.view.frame.size.width-10, 230.0)];
    //  Create a CAShapeLayer that uses that UIBezierPath:
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[UIColor lightGrayColor] CGColor];
    shapeLayer.lineWidth = 2.0;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    
    [self.view.layer addSublayer:shapeLayer];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.delegate = self;
    [self.view addSubview:HUD];
    [self.view addSubview:self.receiveBillView];
    [self.view addSubview:self.configureStepView];
    self.receiveBillView.hidden = TRUE;
    self.configureStepView.hidden = TRUE;
}

-(void)viewDidAppear:(BOOL)animated {
    
    [self drawLine];
    [super viewDidAppear:YES];
    
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)ReceiveBill_Clicked:(id)sender {
   // [self.view addSubview:self.receiveBillView];

    [self.receiveBillView open];
}

- (IBAction)Create_Clicked:(id)sender {
    HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cartoon.png"]];
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.labelColor = [self colorFromHexString:@"#f15a29"];
    HUD.detailsLabelColor = [self colorFromHexString:@"#f15a29"];
    HUD.labelText = @"Kindly Bear with me...";
    HUD.detailsLabelFont = [UIFont systemFontOfSize:13];
    HUD.detailsLabelText = @" While i am creating a bucket for you, \rIt will take few seconds...";
    HUD.color = [UIColor clearColor];
    HUD.backgroundColor = [UIColor whiteColor];
    [HUD show:YES];
    S3GetACLResponse *aclResponse = [[AmazonClientManager s3] getACL:[[S3GetACLRequest alloc] init]];
    S3AccessControlList *accesslist = aclResponse.acl;
    S3Owner *owner = accesslist.owner;
    ownerName = owner.displayName;
    ownerName = [ownerName substringToIndex: MIN(6, [ownerName length])];
    [self createBucket];
    
}

- (void) createBucket {
    @try {
        NSTimeInterval  today = [[NSDate date] timeIntervalSince1970];
        NSString *intervalString = [NSString stringWithFormat:@"%.0f", today];
        NSString *bucketName = [NSString stringWithFormat:@"Amaze-%@-%@",intervalString, ownerName];
        bucketName = [bucketName lowercaseString];
        [[AmazonClientManager s3] createBucket:[[S3CreateBucketRequest alloc] initWithName:bucketName]];
        [self setAmazeBucketPermissions:bucketName];
        [ConfigureStepView sharedInstance].bucketNameString = bucketName;
        [self.configureStepView open];
        [self.createButton setEnabled:NO];
        [self.createButton setBackgroundColor:[UIColor lightGrayColor]];
        [self.createButton setTitle:@"Created" forState:UIControlStateNormal];
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"BucketCreated" forKey:kAppStatus];
        [defaults synchronize];
    }
    @catch (AmazonClientException *exception) {
        errorString = exception.message;
        if ([errorString containsString:@"bucket name is not available"]) {
            errorString = @"Bucket name already exists.";
        }
        else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error Creating Bucket"
                                                                                     message:errorString
                                                                              preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    [HUD hide:YES];
}
-(void)setAmazeBucketPermissions : (NSString *)bucketName {
    @try {
        S3BucketPolicy *bucketPolicy = [S3BucketPolicy new];
        bucketPolicy.policyText =[NSString stringWithFormat:@"{\"Version\":\"2008-10-17\",\"Id\":\"Policy1335892530063\",\"Statement\":[{\"Sid\":\"Stmt1335892150622\",\"Effect\":\"Allow\",\"Principal\":{\"AWS\":\"arn:aws:iam::386209384616:root\"},\"Action\":[\"s3:GetBucketAcl\",\"s3:GetBucketPolicy\"],\"Resource\":\"arn:aws:s3:::%@\"},{\"Sid\":\"Stmt1335892526596\",\"Effect\":\"Allow\",\"Principal\":{\"AWS\":\"arn:aws:iam::386209384616:root\"},\"Action\":[\"s3:PutObject\"],\"Resource\":\"arn:aws:s3:::%@/*\"}]}",bucketName,bucketName];
        S3SetBucketPolicyRequest *setRequest = [S3SetBucketPolicyRequest new];
        setRequest.bucket = bucketName;
        setRequest.policy = bucketPolicy;
        
        [[AmazonClientManager s3] setBucketPolicy:setRequest];
        
    }
    @catch (AmazonClientException *exception) {
        errorString = exception.message;
        NSLog(@"Bucket Policy Error: %@", exception);
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error Setting Bucket Policy"
                                                                                 message:errorString
                                                                          preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleCancel
                                                             handler:nil];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)viewDidLayoutSubviews {
    [[RecieveBillView sharedInstance].scrollView setContentSize:CGSizeMake(320 , 800)];
   // [self.scrollView setContentSize:];
    
}

- (void) configurationDismissed {
    self.configureStepView.hidden = TRUE;
}
- (void) recieveBillDismissed {

    self.receiveBillView.hidden = TRUE;
}
@end
