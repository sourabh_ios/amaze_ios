//
//  ViewController.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 01/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//
//

//With No Bucket Access: AKIAJVL4HFICCYIZWXAQ
//Secret: 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
#import "ViewController.h"
#import "Constants.h"
#import "AmazonClientManager.h"
#import "JNKeychain.h"
#import "MBProgressHUD.h"
#import "BucketListViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController () <MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    viewShifted = FALSE;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deFocus)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.delegate = self;
    [self.view addSubview:HUD];
    
    _scannerView.hidden = TRUE;
    _captureSession = nil;
    _isReading = NO;
    [self loadBeepSound];
    // Do any additional setup after loading the view, typically from a nib.
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"BG.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [self colorFromHexString:@"#f15a29"];
    [self.navigationItem setHidesBackButton:YES];

    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.navigationController.navigationBar.hidden = TRUE;
    UIColor *themeColor = [self colorFromHexString:@"#f15a29"];
    self.accessKeyTextfield.layer.borderColor = [themeColor CGColor];
    self.accessKeyTextfield.layer.borderWidth=1.0;
    self.secretKeyTextField.layer.borderColor = [themeColor CGColor];
    self.secretKeyTextField.layer.borderWidth=1.0;
    
    self.cancelButton.layer.cornerRadius = 1.5;
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : themeColor };
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc]initWithString:@" Access Key (20 Characters) " attributes:attrs];
    NSRange range=[@"Access Key (20 Characters) " rangeOfString:@"(20 Characters) "];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:range];

    NSMutableAttributedString * string1 = [[NSMutableAttributedString alloc]initWithString:@" Secret Key (40 Characters) " attributes:attrs];
    NSRange range1=[@"Secret Key (40 Characters) " rangeOfString:@"(40 Characters) "];
    [string1 addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:range1];

    self.accessKeyTextfield.attributedPlaceholder = string;
    self.secretKeyTextField.attributedPlaceholder = string1;
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* appStatus =    [defaults objectForKey:kAppStatus];
    if ([appStatus isEqualToString:@"KeysReceived"]) {
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(autoLogin) userInfo:nil repeats:NO];
    }
    [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(autoFill) userInfo:nil repeats:NO];

    
}

- (void) autoLogin {
    NSString *accessKey = [JNKeychain loadValueForKey:kAccessKey];
    NSString *secretKey = [JNKeychain loadValueForKey:kSecretKey];
    self.accessKeyTextfield.text = accessKey;
    self.secretKeyTextField.text = secretKey;
    [self Login_Clicked:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

- (IBAction)Login_Clicked:(id)sender {

    HUD.labelText = @"Verifying Keys...";
    [HUD show:YES];
    [JNKeychain saveValue:self.accessKeyTextfield.text forKey:kAccessKey];
    [JNKeychain saveValue:self.secretKeyTextField.text forKey:kSecretKey];
    
    bucketList = [self bucketListArray];
    if (bucketList == nil) {
        if ([errorString isEqualToString:@"Access Denied"]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:errorString
                                                                                     message:@"Please ask administrator to grant s3 bucket access"
                                                                              preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                                             style:UIAlertActionStyleCancel
                                                                           handler:nil];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if ([errorString containsString:@"The Internet connection appears to be offline"]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"The Internet connection appears to be offline"
                                                                                     message:@"Please check your Internet Settings"
                                                                              preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if ([errorString containsString:@"The request timed out."]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Request timed out"
                                                                                     message:@"Please try again"
                                                                              preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if ([errorString containsString:@"SSL error"]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Server Connection Error"
                                                                                     message:@"Please try again"
                                                                              preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                                   style:UIAlertActionStyleCancel
                                                                 handler:nil];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:errorString
                                                                                     message:@"Please provide valid credentials"
                                                                              preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                                             style:UIAlertActionStyleCancel
                                                                           handler:nil];
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    else {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"KeysReceived" forKey:kAppStatus];
        [defaults synchronize];
        if (bucketList.count) {
           [self performSegueWithIdentifier:@"pushToBucket" sender:sender];
         //   [self performSegueWithIdentifier:@"pushConfigureSegue" sender:sender];

        }
        else {
            [self performSegueWithIdentifier:@"pushConfigureSegue" sender:sender];
        }
    }
    [HUD hide:YES];
}


-(NSArray *)bucketListArray {
    NSArray *array;
    @try {
        array = [[AmazonClientManager s3] listBuckets];
    }
    @catch (AmazonClientException *exception) {
        //NSLog(@"Exception = %@", exception);
        errorString = exception.message;
        array = nil;
    }
    return array;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"pushToBucket"])
    {
        BucketListViewController *vc = (BucketListViewController *)[[segue destinationViewController] topViewController];
        [vc setBucketList:bucketList];
        self.navigationController.navigationBar.hidden = FALSE;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self hideKeyBoard];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self hideKeyBoard];
    return YES;
}

- (void)hideKeyBoard {
    if (viewShifted == TRUE) {
        viewShifted = FALSE;
        [UIView animateWithDuration:.3 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+shiftDistance, self.view.frame.size.width, self.view.frame.size.height);
            
        } completion:Nil];
    }
    [self.accessKeyTextfield resignFirstResponder];
    [self.secretKeyTextField resignFirstResponder];
}

- (void)deFocus {
    [self hideKeyBoard];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    int height = MIN(keyboardSize.height,keyboardSize.width);
    int keyBoardOrigin = self.view.frame.size.height - height;
    int buttonHeight = self.loginButton.frame.origin.y;
    
    if (buttonHeight > keyBoardOrigin && viewShifted == FALSE) {
        viewShifted = TRUE;
        shiftDistance = buttonHeight-keyBoardOrigin;
        [UIView animateWithDuration:.3 animations:^{
            self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-shiftDistance, self.view.frame.size.width, self.view.frame.size.height);
            
        } completion:Nil];
    }
}

- (IBAction)Help_Clicked:(id)sender {
    
    UIAlertView *anAlert = [[UIAlertView alloc] initWithTitle:@"HELP!" message:@"Please Get Your Security Credentials ( Access Key and Secret Key ) From Your IAM Management Console!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [anAlert show];
}

#warning TODO: Temporary keys, to be deleted
- (void)autoFill{
   //   self.accessKeyTextfield.text = @"AKIAJSQJN6KNTUA4DJGA";
   // self.secretKeyTextField.text = @"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";
    self.accessKeyTextfield.text = @"AKIAIN26RGH6YFYU65JA";
    self.secretKeyTextField.text = @"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";
}


#pragma mark - QR Scanner method implementation
-(void)redLineTopToBottom {
    [NSTimer scheduledTimerWithTimeInterval:1.0f
                                     target:self
                                   selector:@selector(redLineBottomToTop)
                                   userInfo:nil
                                    repeats:NO];
    [UIView animateWithDuration:.8 animations:^{
        _redLineImageView.center = CGPointMake(_redLineImageView.center.x, _redLineImageView.center.y+190);
    } completion:Nil];
}

-(void)redLineBottomToTop {
    [UIView animateWithDuration:.8 animations:^{
        _redLineImageView.center = CGPointMake(_redLineImageView.center.x, _redLineImageView.center.y-190);
    } completion:Nil];
    
}
- (IBAction)startStopReading:(id)sender {
    
    _scannerView.hidden = FALSE;
    scannerTimer = [NSTimer scheduledTimerWithTimeInterval:2.0f
                                                    target:self
                                                  selector:@selector(redLineTopToBottom)
                                                  userInfo:nil
                                                   repeats:YES];
    if (!_isReading) {
        // This is the case where the app should read a QR code when the start button is tapped.
        if ([self startReading]) {
        }
    }
    else{
        // In this case the app is currently reading a QR code and it should stop doing so.
        [self stopReading];
    }
    
    // Set to the flag the exact opposite value of the one that currently has.
    _isReading = !_isReading;
}

- (IBAction)cancelReading:(id)sender {
    [self stopReading];
    _isReading = NO;
}

#pragma mark - Private method implementation

- (BOOL)startReading {
    NSError *error;
    
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        //NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize the captureSession object.
    _captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_scannerViewCamera.layer.bounds];
    [_scannerViewCamera.layer addSublayer:_videoPreviewLayer];
    
    
    // Start video capture.
    [_captureSession startRunning];
    
    return YES;
}


-(void)stopReading{
    [scannerTimer invalidate];
    // Stop video capture and make the capture session object nil.
    [_captureSession stopRunning];
    _captureSession = nil;
    _scannerView.hidden = TRUE;
    // Remove the video preview layer from the viewPreview view's layer.
    [_videoPreviewLayer removeFromSuperlayer];
}


-(void)loadBeepSound{
    // Get the path to the beep.mp3 file and convert it to a NSURL object.
    NSString *beepFilePath = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    NSURL *beepURL = [NSURL URLWithString:beepFilePath];
    
    NSError *error;
    // Initialize the audio player object using the NSURL object previously set.
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:beepURL error:&error];
    if (!error) {
        [_audioPlayer prepareToPlay];
    }
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            outPutString = [metadataObj stringValue];
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            _isReading = NO;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSData *webData = [outPutString dataUsingEncoding:NSUTF8StringEncoding];
                NSError *error;
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:webData options:0 error:&error];
                NSString *acesKey = [jsonDict valueForKey:@"aws_access_key"];
                NSString *secKey = [jsonDict valueForKey:@"aws_secret_key"];
                if (acesKey.length == 20 && secKey.length == 40) {
                    self.accessKeyTextfield.text = acesKey;
                    self.secretKeyTextField.text = secKey;
                    [self Login_Clicked:self];
                }
                else {
                    self.accessKeyTextfield.text = @"";
                    self.secretKeyTextField.text = @"";
                    UIAlertView *anAlert = [[UIAlertView alloc] initWithTitle:@"Invalid Code" message:@"Please scan valid Amaze QR Code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         [anAlert show];
                }
            });
            

            if (_audioPlayer) {
                [_audioPlayer play];
            }
        }
    }    
}

@end
