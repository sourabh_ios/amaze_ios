//
//  ViewController.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 01/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AWSiOSSDK/S3/AmazonS3Client.h>
#import <AVFoundation/AVFoundation.h>


@interface ViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>
{
    NSArray *bucketList;
    NSString *errorString;
    BOOL viewShifted;
    int shiftDistance;
    NSTimer *scannerTimer;
    NSString *outPutString;
}

@property (weak, nonatomic) IBOutlet UITextField *accessKeyTextfield;
@property (weak, nonatomic) IBOutlet UITextField *secretKeyTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;


@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic) BOOL isReading;
@property (weak, nonatomic) IBOutlet UILabel *holdCameraLbl;
@property (weak, nonatomic) IBOutlet UILabel *qrScannerViewportLbl;

@property (weak, nonatomic) IBOutlet UIView *scannerView;
@property (weak, nonatomic) IBOutlet UIView *scannerViewCamera;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIImageView *redLineImageView;
@end

