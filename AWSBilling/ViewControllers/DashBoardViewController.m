//
//  DashBoardViewController.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 10/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import "DashBoardViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "DashCollectionViewCell.h"
#import "DashTableViewCell.h"
#import "LeftPanel.h"
#import "RightPanel.h"
#import "Users.h"
#import "Product.h"
#import "Bill.h"
#import "BillDetail.h"
#import "DetailViewController.h"
#import <AWSiOSSDK/S3/AmazonS3Client.h>
#import "AmazonClientManager.h"
#import "Constants.h"
#import "UsageDetailViewController.h"
#import "CHCSVParser.h"
#import "MBProgressHUD.h"
#import "JNKeychain.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "CDatePickerViewEx.h"

@interface DashBoardViewController () <CHCSVParserDelegate, MBProgressHUDDelegate>
{
    Bill *monthCSVBill;
    Product *productCSV;
    MBProgressHUD *HUD;
}

@property (nonatomic, weak) IBOutlet CDatePickerViewEx *picker;
@property (nonatomic, strong) CDatePickerViewEx *pickerFromCode;

@end

@implementation DashBoardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 /*
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSDirectoryEnumerator *enumerator = [fileManager enumeratorAtPath:documentsDirectory];
    NSString *path;
*/
    self.dashBoardList = [[NSMutableArray alloc] init];
    self.linkCSVAccounts = [[NSMutableArray alloc] init];
 /*   while (path = [enumerator nextObject]) {
        monthCSVBill = [[Bill alloc] init];
        monthCSVBill.monthName = [path stringByReplacingOccurrencesOfString:@".csv" withString:@""];;
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:path];
        NSData *data = [NSData dataWithContentsOfFile:filePath];
        [self csvParseWithString:data];
    }
    
    self.csvData = self.dashBoardList;
   
    self.linkAccounts = self.linkCSVAccounts;
  */
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.delegate = self;
    [self.view addSubview:HUD];
    
    self.navigationController.navigationBar.barTintColor = [self colorFromHexString:@"#f15a29"];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    self.dashCollectionView.delegate = self;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.dashCollectionView setTag:101];
    UIImage* image3 = [UIImage imageNamed:@"menu-button.png"];
    CGRect frameimg = CGRectMake(0, 0, image3.size.width, image3.size.height);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(leftPanelButton:)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIImage* image4 = [UIImage imageNamed:@"3Dots.png"];
    CGRect frameimg1 = CGRectMake(0, 0, image3.size.width, image3.size.height);
    UIButton *someButton1 = [[UIButton alloc] initWithFrame:frameimg1];
    [someButton1 setBackgroundImage:image4 forState:UIControlStateNormal];
    [someButton1 addTarget:self action:@selector(rightPanelButton:)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton1 setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=mailbutton;
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:someButton1];
    self.navigationItem.rightBarButtonItem=mailbutton1;
    
    [self.productListTable setSeparatorColor:[self colorFromHexString:@"#f15a29"]];
    self.productListTable.contentInset = UIEdgeInsetsMake(-16, 0, 0, 0);

    self.topProductsLabel.textColor = [self colorFromHexString:@"#f15a29"];
    [self.dashCollectionView.layer setBorderColor:[self colorFromHexString:@"#f15a29"].CGColor];
    [self.dashCollectionView.layer setBorderWidth:1.1f];
    
    // Do any additional setup after loading the view.
    [self.pieChart setDataSource:self];
    [self.pieChart setAnimationSpeed:1.0];
    [self.pieChart setLabelRadius:70];
    [self.pieChart setLabelColor:[UIColor whiteColor]];
    [self.pieChart setShowPercentage:NO];
    [self.pieChart setShowLabel:YES];
    [self.pieChart setPieBackgroundColor:[UIColor colorWithWhite:0.95 alpha:1]];
    [self.pieChart setUserInteractionEnabled:NO];
    [self.pieChart setLabelShadowColor:[UIColor blackColor]];
    [self.pieChart setLabelFont:[UIFont systemFontOfSize:12]];
    
    self.sliceColors =[NSArray arrayWithObjects:
                       [UIColor colorWithRed:246/255.0 green:155/255.0 blue:0/255.0 alpha:1],
                       [UIColor colorWithRed:129/255.0 green:195/255.0 blue:29/255.0 alpha:1],
                       [UIColor colorWithRed:62/255.0 green:173/255.0 blue:219/255.0 alpha:1],
                       [UIColor colorWithRed:229/255.0 green:66/255.0 blue:115/255.0 alpha:1],
                       [UIColor colorWithRed:148/255.0 green:141/255.0 blue:139/255.0 alpha:1],nil];
    [self.circleLabel.layer setCornerRadius:50];
    [self drawLine];
    
//    [self getDetails:@""];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(leftPanelDismissed) name:@"LeftPanelDismissed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rightPanelDismissed) name:@"RightPanelDismissed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logOutApp) name:@"LogOutTapped" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bucketChangedTap) name:@"BucketChangedTapped" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUIAfterMonthFromNotication:) name:@"RefreshPreviousMonths" object:nil];

    _headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.dashCollectionView.frame.origin.x, self.dashCollectionView.frame.origin.y-33, self.pieChart.frame.size.width, 30)];
    [self.dashScrollView addSubview:_headerLabel];
    _headerLabel.textColor = [self colorFromHexString:@"#f15a29"];
    [_headerLabel setFont:[UIFont systemFontOfSize:13]];
    [_headerLabel setTextAlignment:NSTextAlignmentCenter];
    
    
    NSTimeInterval  today = [[NSDate date] timeIntervalSince1970];
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastUdated = [defaults objectForKey:kLastUpdated];
    float lastUp = [lastUdated floatValue];
    int difference = today - lastUp;
    if (difference > 10800) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [self backGroundUpdate];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateUIAfterMonth:selectedMonth];
            });
        });
    }
    NSDate *date = [NSDate date];
    NSDateFormatter *datFormatter = [[NSDateFormatter alloc] init];
    [datFormatter setDateFormat:@"yyyy-MM"];
    selectedMonth = [datFormatter stringFromDate:date];
    //selectedMonth = @"2014-11";
    [self updateUIAfterMonth:selectedMonth];
/*    [self.dashCollectionView reloadData];
    NSIndexPath *newIndexPath;
    if (self.monthList.count > 6)
        newIndexPath = [NSIndexPath indexPathForItem:5 inSection:0];
    else
        newIndexPath = [NSIndexPath indexPathForItem:self.monthList.count-1 inSection:0];

    [self.dashCollectionView scrollToItemAtIndexPath:newIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    [self.dashCollectionView deselectItemAtIndexPath:newIndexPath animated:NO];
*/
    
    [self.picker selectToday];
    //CGRect frame = self.picker.bounds;
   // frame.origin.y = self.picker.frame.size.height;
    
}


-(void) backGroundUpdate {
    @try {
        NSTimeInterval  today = [[NSDate date] timeIntervalSince1970];
        NSString *intervalString = [NSString stringWithFormat:@"%.0f", today];
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        S3GetObjectRequest  *getObjectRequest;
        S3GetObjectResponse *getObjectResponse;
        NSString* bucketName =    [defaults objectForKey:kBucketName];
        S3ListObjectsRequest  *listObjectRequest = [[S3ListObjectsRequest alloc] initWithName:bucketName];
        
        S3ListObjectsResponse *listObjectResponse = [[AmazonClientManager s3] listObjects:listObjectRequest];
        S3ListObjectsResult   *listObjectsResults = listObjectResponse.listObjectsResult;
        
        
        if (bucketFiles == nil) {
            bucketFiles = [[NSMutableArray alloc] initWithCapacity:[listObjectsResults.objectSummaries count]];
        }
        else {
            [bucketFiles removeAllObjects];
        }
        for (S3ObjectSummary *objectSummary in listObjectsResults.objectSummaries) {
            
            if ([[objectSummary key] rangeOfString:@"-aws-billing-csv-"].location != NSNotFound)
                [bucketFiles addObject:[objectSummary key]];
        }
        [bucketFiles sortUsingSelector:@selector(compare:)];
        
        
        if (bucketFiles.count == 0) {

        }
        else {
            getObjectRequest = [[S3GetObjectRequest alloc] initWithKey:[bucketFiles objectAtIndex:bucketFiles.count-1] withBucket:bucketName];
                getObjectResponse = [[AmazonClientManager s3] getObject:getObjectRequest];
                
                NSString *subString = [[bucketFiles objectAtIndex:bucketFiles.count-1]  stringByReplacingOccurrencesOfString:@".csv" withString:@""];
                subString = [subString substringFromIndex:[subString length]-7];
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *fileExten = [NSString stringWithFormat:@"%@.csv",subString];
                NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileExten];
                
                [getObjectResponse.body writeToFile:filePath atomically:YES];
        }
        [defaults setObject:intervalString forKey:kLastUpdated];
    }
    @catch (AmazonClientException *exception) {
        NSLog(@"Exception = %@", exception);
    }
}

- (void)getDetails: (NSString *) userName {

    self.monthList = [[NSMutableArray alloc] init];
    for (Bill *monthBill in self.csvData) {
        NSArray *monthArray = monthBill.monthDetails;
        NSMutableArray *nameArray = [[NSMutableArray alloc] init];
        NSMutableDictionary *finalDictionary = [[NSMutableDictionary alloc] init];
        BillDetail *billDetail = [[BillDetail alloc] init];
        float billAmount = 0;
        NSString *currency;
        for (Product *product in monthArray ) {
            if ([product.linkedAccount isEqualToString:userName]) {
                if (![nameArray containsObject:product.productName]) {
                    [nameArray addObject:product.productName];
                    NSMutableArray *temp = [[NSMutableArray alloc] init];
                    [temp addObject:product];
                    [finalDictionary setObject:temp forKey:product.productName];
                    billDetail.payerName = product.linkedAccount;
                    billDetail.payerID = product.linkID;
                }
                else {
                    NSMutableArray *temp = [[NSMutableArray alloc] init];
                    temp = [finalDictionary objectForKey:product.productName];
                    [temp addObject:product];
                    [finalDictionary setObject:temp forKey:product.productName];
                }
                billAmount = billAmount + product.productPrice;
                currency = product.currency;
            }
        }
        billDetail.monthName = monthBill.monthName;
        billDetail.payerAmount = [NSString stringWithFormat:@"%.2f %@", billAmount, currency];
        billDetail.productsNameArray = nameArray;
        billDetail.productsDetailsDictionary = finalDictionary;
        if (billDetail.payerID != nil) {
            if ([billDetail.payerID isEqualToString:@""]) {
                billDetail.payerName = self.payerName;
                billDetail.payerID = self.payerID;
            }
            [self.monthList addObject:billDetail];
        }
    }
    if (self.monthList.count > 1) {
        _leftArrow.enabled = YES;
        [_leftArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-left.png"] forState:UIControlStateNormal];

        _rightArrow.enabled = NO;
        [_rightArrow setBackgroundImage:[UIImage imageNamed:@"orange-towards-right.png"] forState:UIControlStateNormal];

    }
    else {
        _leftArrow.enabled = NO;
        [_leftArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-left.png"] forState:UIControlStateNormal];

        _rightArrow.enabled = NO;
        [_rightArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-right.png"] forState:UIControlStateNormal];
    }
}

- (void) drawLine {
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(10.0, 180.0)];
    [path addLineToPoint:CGPointMake(self.view.frame.size.width-10, 180.0)];
    //  Create a CAShapeLayer that uses that UIBezierPath:
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path CGPath];
    shapeLayer.strokeColor = [[UIColor lightGrayColor] CGColor];
    shapeLayer.lineWidth = 2.0;
    shapeLayer.fillColor = [[UIColor clearColor] CGColor];
    
    [self.dashScrollView.layer addSublayer:shapeLayer];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
  [self.productListTable setBackgroundColor:[UIColor clearColor]];
    [super viewDidAppear:YES];
    [_headerLabel setCenter:CGPointMake(self.pieChart.center.x, _headerLabel.center.y)];

}

-(void)viewWillAppear:(BOOL)animated {
  //  self.navigationItem.title = @"Dashboard";
    [super viewWillAppear:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - XYPieChart Data Source


-(NSString *)pieChart:(XYPieChart *)pieChart textForSliceAtIndex:(NSUInteger)index {
    Product *tempProduct = [[Product alloc] init];
    tempProduct = [self.slices objectAtIndex:index];
    float totalPrice = 0;
    for (Product *prod in self.slices ) {
        totalPrice = totalPrice + prod.productPrice;
    }
    float percentage = tempProduct.productPrice/totalPrice *100;
   // NSString *percentString = [NSString stringWithFormat:@"%."]
    NSString *labelString =[NSString stringWithFormat:@"%@",tempProduct.abbrevatedName];
    labelString = [labelString substringToIndex: MIN(4, [labelString length])];
    NSString *percentString = [NSString stringWithFormat:@" %.1f", percentage];
    percentString = [percentString stringByAppendingString:@"%"];
    labelString = [labelString stringByAppendingString:percentString];
    return labelString;
}

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return self.slices.count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    Product *tempProduct = [[Product alloc] init];
    tempProduct = [self.slices objectAtIndex:index];
    NSString *tempString = [NSString stringWithFormat:@"%.2f",tempProduct.productPrice];
    float value = [tempString floatValue];
    return value;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    return [self.sliceColors objectAtIndex:(index % self.sliceColors.count)];
}

#pragma mark - Table View Data Source
- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}


- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    if (self.productList.count < 5)
        return self.productList.count;
    
    else
       return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"DashTableCell";
    
    DashTableViewCell *dashTableCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [dashTableCell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    Product *product = [self.productList objectAtIndex:indexPath.row];
    if ([product.currency isEqualToString:@"USD"])
        product.currency = @"$";
    
    product.productAmount = [NSString stringWithFormat:@"%@ %.2f",product.currency,product.productPrice];
    [dashTableCell populateCell:product.abbrevatedName   withAmount:product.productAmount];
    dashTableCell.backgroundColor = [UIColor clearColor];
    return dashTableCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Product *product;
    product = [self.productList objectAtIndex:indexPath.row];
    self.productDetailsList = product.productDetails;
    productAmountString = [NSString stringWithFormat:@"%@ %.2f",product.currency,product.productPrice];
    [self performSegueWithIdentifier:@"pushDashToUsage" sender:self];
}

#pragma mark - Collection View Data Source

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if (self.monthList.count > 6)
        return 6;
    else
        return self.monthList.count;
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DashCollectionViewCell *dashCell = [cv dequeueReusableCellWithReuseIdentifier:@"DashCell" forIndexPath:indexPath];
    BillDetail *billDetail = [[BillDetail alloc] init];
    billDetail = [self.monthList objectAtIndex:indexPath.row];
    [dashCell populateCell:billDetail.payerName withAmount:billDetail.payerAmount];
    
    NSString *yearMonth = billDetail.monthName;
    NSArray *monthArray = [yearMonth componentsSeparatedByString:@"-"];
    if (monthArray.count == 2) {
        NSString *year = [monthArray objectAtIndex:0];
        NSString *month = [monthArray objectAtIndex:1];
        if ([month isEqualToString:@"01"])
            month = @"January";
        else if ([month isEqualToString:@"02"])
            month = @"Febraury";
        else if ([month isEqualToString:@"03"])
            month = @"March";
        else if ([month isEqualToString:@"04"])
            month = @"April";
        else if ([month isEqualToString:@"05"])
            month = @"May";
        else if ([month isEqualToString:@"06"])
            month = @"June";
        else if ([month isEqualToString:@"07"])
            month = @"July";
        else if ([month isEqualToString:@"08"])
            month = @"August";
        else if ([month isEqualToString:@"09"])
            month = @"September";
        else if ([month isEqualToString:@"10"])
            month = @"October";
        else if ([month isEqualToString:@"11"])
            month = @"November";
        else if ([month isEqualToString:@"12"])
            month = @"December";
        
        _headerLabel.text = [NSString stringWithFormat:@"%@ %@ bill amount", month, year];
    }
    //self.productList = billDetail.productsNameArray;
    self.productList = [[NSMutableArray alloc] init];
    self.slices = [[NSMutableArray alloc] init];

    [self getProductList:billDetail.productsDetailsDictionary];
    [self.productListTable reloadData];
    [self.pieChart reloadData];
    [self viewDidLayoutSubviews];
    return dashCell;
}

- (void)getProductList : (NSDictionary *)detailDictionary {
 
    
    NSArray *nameArray = [detailDictionary allKeys];
    for (int i = 0 ; i < [nameArray count] ; i++) {
        Product *prod = [[Product alloc] init];
        NSArray *tempArray = [detailDictionary valueForKey:[nameArray objectAtIndex:i]];
        for (Product *tempProduct in tempArray) {
            prod.productPrice = prod.productPrice + tempProduct.productPrice;
            prod.productName = tempProduct.productName;
            prod.abbrevatedName = tempProduct.abbrevatedName;
            prod.imageName = tempProduct.imageName;
            prod.currency = tempProduct.currency;
            prod.billingPeriodStart = tempProduct.billingPeriodStart;
            prod.billingPeriodEnd = tempProduct.billingPeriodEnd;
        }
        if (prod.productPrice > 0) {
            [self.slices addObject:prod];
        }
        prod.productDetails = [tempArray mutableCopy];
        [self.productList addObject:prod];
    }
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"productPrice"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [self.productList sortedArrayUsingDescriptors:sortDescriptors];
    sortedArray = [[sortedArray  reverseObjectEnumerator] allObjects];
    self.productList = [sortedArray mutableCopy];
}

- (void)viewDidLayoutSubviews {
    self.tableViewHeightConstraint.constant = self.productListTable.contentSize.height;
    [self.view setNeedsUpdateConstraints];
    self.productListTable.scrollEnabled = NO;
    [self.dashScrollView setContentSize:CGSizeMake(320 , self.productListTable.frame.origin.y + self.productListTable.contentSize.height)];
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

-(void) logOutApp {
   // modalToLogin
    self.view.alpha = 1.0;
    self.navigationController.navigationBar.alpha = 1.0;
    UIAlertView *anAlert = [[UIAlertView alloc] initWithTitle:@"Log Out" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
    anAlert.tag = 3;
    [anAlert show];
    
 /*    */
    
}

- (void) bucketChangedTap {
    self.view.alpha = 1.0;
    self.navigationController.navigationBar.alpha = 1.0;
    UIAlertView *anAlert = [[UIAlertView alloc] initWithTitle:@"Bucket Change" message:@"Are you sure you have changed your Billing Bucket?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
    anAlert.tag = 4;
    [anAlert show];
  /*  */
}

- (void) refreshPreviousMonths {
    accountNameString = [[LeftPanel sharedInstance] replyAccountName];
    if (accountNameString == nil)
        accountNameString = @"";
    
    [self getDetails:accountNameString];
   // self.monthList = [[self.monthList reverseObjectEnumerator] allObjects];
    [self.dashCollectionView reloadData];
    NSIndexPath *newIndexPath;
    if (self.monthList.count > 6)
        newIndexPath = [NSIndexPath indexPathForItem:5 inSection:0];
    else
        newIndexPath = [NSIndexPath indexPathForItem:self.monthList.count-1 inSection:0];
    [self.dashCollectionView scrollToItemAtIndexPath:newIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    [self.dashCollectionView deselectItemAtIndexPath:newIndexPath animated:NO];
    if (self.monthList.count > 1) {
        _leftArrow.enabled = YES;
        [_leftArrow setBackgroundImage:[UIImage imageNamed:@"orange-towards-left.png"] forState:UIControlStateNormal];
        
        _rightArrow.enabled = NO;
        [_rightArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-right.png"] forState:UIControlStateNormal];
        
    }
    else {
        _leftArrow.enabled = NO;
        [_leftArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-left.png"] forState:UIControlStateNormal];
        
        _rightArrow.enabled = NO;
        [_rightArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-right.png"] forState:UIControlStateNormal];
    }

}

-(void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 3) {
        if (buttonIndex == 1) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSFileManager *fileMgr = [[NSFileManager alloc] init];
            NSError *error = nil;
            NSArray *directoryContents = [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error];
            if (error == nil) {
                for (NSString *path in directoryContents) {
                    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:path];
                    BOOL removeSuccess = [fileMgr removeItemAtPath:fullPath error:&error];
                    if (!removeSuccess) {
                        NSLog(@"Files Not deleted");
                    }
                }
            }
            
            [JNKeychain saveValue:@"" forKey:kAccessKey];
            [JNKeychain saveValue:@"" forKey:kSecretKey];
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@"" forKey:kAppStatus];
            //[LeftPanel sharedInstance]
            AppDelegate * appsDelegate =[[UIApplication sharedApplication] delegate];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController *myViewController= [storyboard instantiateViewControllerWithIdentifier:@"loginStoryBoardController"];
            appsDelegate.window.rootViewController = nil;
            appsDelegate.window.rootViewController = myViewController;        }
    }
    else if (alertView.tag == 4) {
        if (buttonIndex == 1) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSFileManager *fileMgr = [[NSFileManager alloc] init];
            NSError *error = nil;
            NSArray *directoryContents = [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error];
            if (error == nil) {
                for (NSString *path in directoryContents) {
                    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:path];
                    BOOL removeSuccess = [fileMgr removeItemAtPath:fullPath error:&error];
                    if (!removeSuccess) {
                        NSLog(@"Files Not deleted");
                    }
                }
            }
            
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@"KeysReceived" forKey:kAppStatus];
            AppDelegate * appsDelegate =[[UIApplication sharedApplication] delegate];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UINavigationController *myViewController= [storyboard instantiateViewControllerWithIdentifier:@"loginStoryBoardController"];
            appsDelegate.window.rootViewController = nil;
            appsDelegate.window.rootViewController = myViewController;
        }
    }
}

- (IBAction)PickDates:(id)sender {
    self.dashScrollView.alpha = 0.4;
    self.picker.hidden = FALSE;
    self.datePickerView.hidden = FALSE;
    self.picker.backgroundColor = [UIColor whiteColor];
}

- (IBAction)DatePicked:(id)sender {
    self.datePickerView.hidden = TRUE;
    self.dashScrollView.alpha = 1.0;
    int idleTime = [self.picker selectedRowInComponent:0];
    int temp = [self.picker selectedRowInComponent:1];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *datFormatter = [[NSDateFormatter alloc] init];
    [datFormatter setDateFormat:@"yy"];
    NSString *curretYear = [datFormatter stringFromDate:date];
    int mod = idleTime%12;
    int mod1 = temp%[curretYear integerValue];
    mod1 = mod1+2000;
    if (mod < 10)
        selectedMonth = [NSString stringWithFormat:@"%d-0%d",mod1+1,mod+1];
    else
        selectedMonth = [NSString stringWithFormat:@"%d-%d",mod1+1,mod+1];
    
    [self updateUIAfterMonth:selectedMonth];
}

#pragma mark - Left Panel Methods
//** Function called to Dismiss left panel **//
-(void)leftPanelDismissed
{
    self.view.alpha = 1.0;
    self.navigationController.navigationBar.alpha = 1.0;
    accountNameString = [[LeftPanel sharedInstance] replyAccountName];
    [self updateUIAfterMonth:selectedMonth];
 /*   if (accountNameString == nil)
        accountNameString = @"";
    
    [self getDetails:accountNameString];
   // self.monthList = [[self.monthList reverseObjectEnumerator] allObjects];
    [self.dashCollectionView reloadData];
    NSIndexPath *newIndexPath;
    if (self.monthList.count > 6)
        newIndexPath = [NSIndexPath indexPathForItem:5 inSection:0];
    else
        newIndexPath = [NSIndexPath indexPathForItem:self.monthList.count-1 inSection:0];
    [self.dashCollectionView scrollToItemAtIndexPath:newIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    [self.dashCollectionView deselectItemAtIndexPath:newIndexPath animated:NO];
    if (self.monthList.count > 1) {
        _leftArrow.enabled = YES;
        [_leftArrow setBackgroundImage:[UIImage imageNamed:@"orange-towards-left.png"] forState:UIControlStateNormal];
        
        _rightArrow.enabled = NO;
        [_rightArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-right.png"] forState:UIControlStateNormal];
        
    }
    else {
        _leftArrow.enabled = NO;
        [_leftArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-left.png"] forState:UIControlStateNormal];
        
        _rightArrow.enabled = NO;
        [_rightArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-right.png"] forState:UIControlStateNormal];
    }
  */
}

-(void)rightPanelDismissed
{
    self.view.alpha = 1.0;
    self.navigationController.navigationBar.alpha = 1.0;
}

//** Function called to Open left menu with sliding animation **//
- (IBAction)leftPanelButton:(id)sender
{
    [LeftPanel sharedInstance].menuItems =[self.linkAccounts mutableCopy];
    [LeftPanel sharedInstance].adminName = self.payerName;
    [LeftPanel sharedInstance].frameHeight = self.view.frame.size.height;
    [[LeftPanel sharedInstance] open];
    self.view.alpha = 0.5;
    self.navigationController.navigationBar.alpha = 0.5;
 
}

- (IBAction)rightPanelButton:(id)sender
{
    [[RightPanel sharedInstance] open];
    self.view.alpha = 0.5;
    self.navigationController.navigationBar.alpha = 0.5;
    
}

//** Function called when right arrow is tapped **//
-(IBAction) scrollRight:(id)sender{
    int targetIndex = (self.dashCollectionView.contentOffset.x/self.dashCollectionView.frame.size.width) + 1;

    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:targetIndex inSection:0];
    [self.dashCollectionView scrollToItemAtIndexPath:newIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    [self.dashCollectionView deselectItemAtIndexPath:newIndexPath animated:NO];
    
}
- (IBAction)Details_Clicked:(id)sender {
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"pushDetailSegue"])
    {
        DetailViewController *detailVC = [segue destinationViewController];
        detailVC.productList = self.productList;
    }
    else if ([[segue identifier] isEqualToString:@"pushDashToUsage"])
    {
        UsageDetailViewController *detailVC = [segue destinationViewController];
        detailVC.productList = self.productDetailsList;
        detailVC.totalAmount = productAmountString;
    }
}

//** Function called when left arrow is tapped **//
-(IBAction)scrollLeft:(id)sender{
    
    int targetIndex = (self.dashCollectionView.contentOffset.x/self.dashCollectionView.frame.size.width) - 1;
    
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:targetIndex inSection:0];
    [self.dashCollectionView scrollToItemAtIndexPath:newIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    [self.dashCollectionView deselectItemAtIndexPath:newIndexPath animated:NO];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView.tag == 101) {
        NSArray *array = [self.dashCollectionView indexPathsForVisibleItems];
        if (array.count > 0) {
            NSIndexPath *indexPath = [array objectAtIndex:0];
             float x = indexPath.row;
            
            BillDetail *billDetail;
            if (x < self.monthList.count)
                billDetail = [self.monthList objectAtIndex:indexPath.row];
            
            NSString *yearMonth = billDetail.monthName;
           
            NSArray *monthArray = [yearMonth componentsSeparatedByString:@"-"];
            if (monthArray.count == 2) {
                NSString *year = [monthArray objectAtIndex:0];
                NSString *month = [monthArray objectAtIndex:1];
                if ([month isEqualToString:@"01"])
                    month = @"January";
                else if ([month isEqualToString:@"02"])
                    month = @"Febraury";
                else if ([month isEqualToString:@"03"])
                    month = @"March";
                else if ([month isEqualToString:@"04"])
                    month = @"April";
                else if ([month isEqualToString:@"05"])
                    month = @"May";
                else if ([month isEqualToString:@"06"])
                    month = @"June";
                else if ([month isEqualToString:@"07"])
                    month = @"July";
                else if ([month isEqualToString:@"08"])
                    month = @"August";
                else if ([month isEqualToString:@"09"])
                    month = @"September";
                else if ([month isEqualToString:@"10"])
                    month = @"October";
                else if ([month isEqualToString:@"11"])
                    month = @"November";
                else if ([month isEqualToString:@"12"])
                    month = @"December";
                
                _headerLabel.text = [NSString stringWithFormat:@"%@ %@ bill amount", month, year];
            }
            //self.productList = billDetail.productsNameArray;
            self.productList = [[NSMutableArray alloc] init];
            self.slices = [[NSMutableArray alloc] init];
            
            [self getProductList:billDetail.productsDetailsDictionary];
            [self.productListTable reloadData];
            [self.pieChart reloadData];
            [self viewDidLayoutSubviews];
        }
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.tag == 101) {
     //   NSLog(@"ContentSize: %f",self.dashCollectionView.contentOffset.x);
     //   NSLog(@"Frame: %f",self.dashCollectionView.frame.size.width);

        if (self.dashCollectionView.contentOffset.x > self.dashCollectionView.frame.size.width-5)
        {
            _leftArrow.enabled = YES;
            [_leftArrow setBackgroundImage:[UIImage imageNamed:@"orange-towards-left.png"] forState:UIControlStateNormal];
        }
        else
        {
            _leftArrow.enabled = NO;
            [_leftArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-left.png"] forState:UIControlStateNormal];
            
        }
        
        float rightMostcontent;
        if (self.monthList.count  > 6)
            rightMostcontent = self.dashCollectionView.frame.size.width * 6 ;
        else
            rightMostcontent = self.dashCollectionView.frame.size.width * (self.monthList.count) ;
        
        if (self.dashCollectionView.contentOffset.x < rightMostcontent-self.dashCollectionView.frame.size.width)
        {
            _rightArrow.enabled = YES;
            [_rightArrow setBackgroundImage:[UIImage imageNamed:@"orange-towards-right.png"] forState:UIControlStateNormal];
            
        }
        else
        {
            _rightArrow.enabled = NO;
            [_rightArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-right.png"] forState:UIControlStateNormal];
            
        }
    }
}
- (void) updateUIAfterMonthFromNotication:(NSNotification *)notification {
    [self updateUIAfterMonth:notification.object];
}
- (void) updateUIAfterMonth:(NSString *)month {
    
    if (month == nil) {
        NSDate *date = [NSDate date];
        NSDateFormatter *datFormatter = [[NSDateFormatter alloc] init];
        [datFormatter setDateFormat:@"yyyy-MM"];
        month = [datFormatter stringFromDate:date];
    }
    [HUD setLabelText:@"Refreshing..."];
    [HUD show:YES];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray  *myArray = (NSArray*)[[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:nil];
    NSString *path;
    BOOL dataTrue;
    [self.dashBoardList removeAllObjects];
    [self.linkCSVAccounts removeAllObjects];
    int MonthCount = 0;
    dataTrue = FALSE;
    myArray = [[myArray reverseObjectEnumerator] allObjects];
    for (path in myArray) {
        monthCSVBill = [[Bill alloc] init];
        monthCSVBill.monthName = [path stringByReplacingOccurrencesOfString:@".csv" withString:@""];;
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:path];
        NSData *data = [NSData dataWithContentsOfFile:filePath];
        if (MonthCount < 6) {
            if ([month isEqualToString:monthCSVBill.monthName]) {
                [self csvParseWithString:data];
                dataTrue = TRUE;
                MonthCount++;
            }
            else if (dataTrue == TRUE) {
                [self csvParseWithString:data];
                MonthCount++;
            }
        }
    }
    if (self.dashBoardList.count != 0) {
        NSArray *tempArray = self.dashBoardList;
        tempArray = [[tempArray reverseObjectEnumerator] allObjects];
        self.dashBoardList = [tempArray mutableCopy];
        self.csvData = self.dashBoardList;
        self.linkAccounts = self.linkCSVAccounts;
        if (accountNameString == nil)
            accountNameString = @"";
        
        [self getDetails:accountNameString];
        [self.dashCollectionView reloadData];
        
        NSIndexPath *newIndexPath;
        if (self.monthList.count > 6)
            newIndexPath = [NSIndexPath indexPathForItem:5 inSection:0];
        else
            newIndexPath = [NSIndexPath indexPathForItem:self.monthList.count-1 inSection:0];
        
        [self.dashCollectionView scrollToItemAtIndexPath:newIndexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        [self.dashCollectionView deselectItemAtIndexPath:newIndexPath animated:NO];
        if (self.monthList.count > 1) {
            _leftArrow.enabled = YES;
            [_leftArrow setBackgroundImage:[UIImage imageNamed:@"orange-towards-left.png"] forState:UIControlStateNormal];
            
            _rightArrow.enabled = NO;
            [_rightArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-right.png"] forState:UIControlStateNormal];
            
        }
        else {
            _leftArrow.enabled = NO;
            [_leftArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-left.png"] forState:UIControlStateNormal];
            
            _rightArrow.enabled = NO;
            [_rightArrow setBackgroundImage:[UIImage imageNamed:@"grey-towards-right.png"] forState:UIControlStateNormal];
        }
    }
    
    else {
        UIAlertView *anAlert = [[UIAlertView alloc] initWithTitle:@"No Billing Data" message:@"Please Select valid Months" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [anAlert show];
        NSDate *date = [NSDate date];
        NSDateFormatter *datFormatter = [[NSDateFormatter alloc] init];
        [datFormatter setDateFormat:@"yyyy-MM"];
        selectedMonth = [datFormatter stringFromDate:date];
        [self updateUIAfterMonth:selectedMonth];
    }

    [HUD hide:YES];
}

#pragma mark - CSV Parser Methods
//
- (void)csvParseWithString : (NSData *)csvData {
    CHCSVParser *csvParser = [[CHCSVParser alloc] initWithCSVString:[[NSString alloc] initWithData:csvData encoding:NSUTF8StringEncoding]];
    csvParser.delegate = self;
    [csvParser parse];
}


- (void)parserDidBeginDocument:(CHCSVParser *)parser{
    
    productCSVList = [[NSMutableArray alloc] init];
    endOfProductList = FALSE;
    billParseEndState = 0;
}


- (void)parserDidEndDocument:(CHCSVParser *)parser{
    for (Product *tempProduct in productCSVList) {
        if (![self.linkCSVAccounts containsObject:tempProduct.linkedAccount]) {
            [self.linkCSVAccounts addObject:tempProduct.linkedAccount];
        }
    }
    monthCSVBill.monthDetails = productCSVList;
    [self.dashBoardList addObject:monthCSVBill];
}


- (void)parser:(CHCSVParser *)parser didBeginLine:(NSUInteger)recordNumber{
    parserLine = recordNumber;
    productCSV = [[Product alloc] init];
}


- (void)parser:(CHCSVParser *)parser didEndLine:(NSUInteger)recordNumber{
    if (parserLine > 1 && !endOfProductList == TRUE) {
        [productCSVList addObject:productCSV];
    }
    else if (endOfProductList == TRUE)
        billParseEndState++;
}


- (void)parser:(CHCSVParser *)parser didReadField:(NSString *)field atIndex:(NSInteger)fieldIndex{
    field = [field stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    if (fieldIndex == 3 && ([field isEqualToString:@"CBRounding"] || [field isEqualToString:@"InvoiceTotal"])) {
        endOfProductList = TRUE;
        if ([field isEqualToString:@"CBRounding"])
            billParseEndState = 0;
        else if ([field isEqualToString:@"InvoiceTotal"])
            billParseEndState = 1;
        
    }
    else if (endOfProductList == TRUE) {
        if (billParseEndState == 1) {
            if (fieldIndex == 23)
                productCSV.currency = field;
            else if (fieldIndex == 28)
                productCSV.productAmount = field;
            else if (fieldIndex == 1)
                self.payerID = field;
            else if (fieldIndex == 8)
                self.payerName = field;
        
            
            monthCSVBill.monthTotal = [productCSV.productAmount stringByAppendingString:productCSV.currency];
            monthCSVBill.monthTotal = [monthCSVBill.monthTotal stringByReplacingOccurrencesOfString:@"\"" withString:@" "];
        }
        
        
    }
    else {
        if (parserLine > 1 && fieldIndex == 13) {
            if ([field isEqualToString:@"Amazon Cognito"]) {
                productCSV.abbrevatedName = @"Cognito";
                productCSV.imageName = @"SimpleIcon_Cognito.png";
            }
            else if ([field isEqualToString:@"Amazon Mobile Analytics"]) {
                productCSV.abbrevatedName = @"Mobile Analytics";
                productCSV.imageName = @"MobileAnalytics.png";
            }
            else if ([field isEqualToString:@"AWS Data Transfer"]) {
                productCSV.abbrevatedName = @"Data Transfer";
                productCSV.imageName = @"AWS.png";
            }
            else if ([field isEqualToString:@"Amazon Simple Notification Service"]) {
                productCSV.abbrevatedName = @"SNS";
                productCSV.imageName = @"SNS.png";
            }
            else if ([field isEqualToString:@"Amazon Simple Storage Service"]) {
                productCSV.abbrevatedName = @"S3";
                productCSV.imageName = @"S3.png";
            }
            else if ([field isEqualToString:@"Amazon Elastic File System"]) {
                productCSV.abbrevatedName = @"EFS";
                productCSV.imageName = @"efs.png";
            }
            else if ([field isEqualToString:@"AWS Storage Gateway"]) {
                productCSV.abbrevatedName = @"Storage Gateway";
                productCSV.imageName = @"StorageGateway.png";
            }
            else if ([field isEqualToString:@"Amazon Glacier"])
                productCSV.abbrevatedName = @"Glacier";
            else if ([field isEqualToString:@"Amazon CloudFront"]) {
                productCSV.abbrevatedName = @"CloudFront";
                productCSV.imageName = @"cloudfront.png";
            }
            else if ([field isEqualToString:@"Amazon Elastic Block Store"]) {
                productCSV.abbrevatedName = @"EBS";
                productCSV.imageName = @"aws-ebs.png";
            }
            else if ([field isEqualToString:@"AWS Directory Service"])
                productCSV.abbrevatedName = @"Directory Services";
            else if ([field isEqualToString:@"Amazon Identity and Access Management"]) {
                productCSV.abbrevatedName = @"IAM";
                productCSV.imageName = @"iam.png";
            }
            else if ([field isEqualToString:@"AWS CloudTrail"])
                productCSV.abbrevatedName = @"CloudTrail";
            else if ([field isEqualToString:@"AWS Config"])
                productCSV.abbrevatedName = @"Config";
            else if ([field isEqualToString:@"Amazon CloudWatch"]) {
                productCSV.abbrevatedName = @"CloudWatch";
                productCSV.imageName = @"cloudwatch.png";
            }
            else if ([field isEqualToString:@"AWS Key Management Service"])
                productCSV.abbrevatedName = @"KMS";
            else if ([field isEqualToString:@"AWS CloudHSM"])
                productCSV.abbrevatedName = @"CloudHSM";
            else if ([field isEqualToString:@"Amazon Elastic Compute Cloud"]) {
                productCSV.abbrevatedName = @"EC2";
                productCSV.imageName = @"aws-ec2.png";
            }
            else if ([field isEqualToString:@"Amazon EC2 Container Service"])
                productCSV.abbrevatedName = @"EC2 Container";
            else if ([field isEqualToString:@"Auto Scaling"])
                productCSV.abbrevatedName = @"Auto Scaling";
            else if ([field isEqualToString:@"Elastic Load Balancing"])
                productCSV.abbrevatedName = @"Load Balancing";
            else if ([field isEqualToString:@"AWS Lambda"])
                productCSV.abbrevatedName = @"Lambda";
            else if ([field isEqualToString:@"Amazon Virtual Private Cloud"])
                productCSV.abbrevatedName = @"VPC";
            else if ([field isEqualToString:@"Amazon Route 53"]) {
                productCSV.abbrevatedName = @"Route 53";
                productCSV.imageName = @"aws-route-53.png";
            }
            else if ([field isEqualToString:@"Amazon Simple Queue Service"]) {
                productCSV.abbrevatedName = @"SQS";
                productCSV.imageName = @"aws-sqs.png";
            }
            else if ([field isEqualToString:@"AWS Direct Connect"])
                productCSV.abbrevatedName = @"Direct Connect";
            else if ([field isEqualToString:@"Amazon WorkSpaces"])
                productCSV.abbrevatedName = @"WorkSpaces";
            else if ([field isEqualToString:@"Amazon WorkSpaces Application Manager"])
                productCSV.abbrevatedName = @"WAM";
            else if ([field isEqualToString:@"Amazon WorkDocs"])
                productCSV.abbrevatedName = @"WorkDocs";
            else if ([field isEqualToString:@"Amazon WorkMail (preview)"])
                productCSV.abbrevatedName = @"WorkMail";
            else if ([field isEqualToString:@"Amazon Elastic MapReduce"]) {
                productCSV.abbrevatedName = @"EMR";
                productCSV.imageName = @"aws-emr.png";
            }
            else if ([field isEqualToString:@"Amazon Kinesis"])
                productCSV.abbrevatedName = @"Kinesis";
            else if ([field isEqualToString:@"AWS Data Pipeline"])
                productCSV.abbrevatedName = @"Data Pipeline";
            else if ([field isEqualToString:@"Amazon Machine Learning"])
                productCSV.abbrevatedName = @"Machine Learning";
            else
                productCSV.abbrevatedName = field;
            
            productCSV.productName = field;
        }
        else if (parserLine > 1 && fieldIndex == 15)
            productCSV.usageType = field;
        else if (parserLine > 1 && fieldIndex == 18)
            productCSV.usageDescription = field;
        else if (parserLine > 1 && fieldIndex == 23)
            productCSV.currency = field;
        else if (parserLine > 1 && fieldIndex == 21)
            productCSV.productQuantity = field;
        
        else if (parserLine > 1 && fieldIndex == 28) {
            productCSV.productAmount = field;
            productCSV.productPrice = [productCSV.productAmount floatValue];
            
        }
        else if (parserLine > 1 && fieldIndex == 9)
            productCSV.linkedAccount = field;
        else if (parserLine > 1 && fieldIndex == 2)
            productCSV.linkID = field;
        else if (parserLine > 1 && fieldIndex == 7) {
            productCSV.invoiceDate = field;
            productCSV.invoiceDate = [productCSV.invoiceDate substringToIndex: MIN(10, [productCSV.invoiceDate length])];
        }
        else if (parserLine > 1 && fieldIndex == 8)
            productCSV.productPayer = field;
        else if (parserLine > 2 && fieldIndex == 10)
            productCSV.productAddress = field;
        else if (parserLine > 1 && fieldIndex == 14)
            productCSV.productSeller = field;
        else if (parserLine > 1 && fieldIndex == 5) {
            productCSV.billingPeriodStart = field;
            productCSV.billingPeriodStart = [productCSV.billingPeriodStart substringToIndex: MIN(10, [productCSV.billingPeriodStart length])];
        }
        else if (parserLine > 1 && fieldIndex == 6) {
            productCSV.billingPeriodEnd = field;
            productCSV.billingPeriodEnd = [productCSV.billingPeriodEnd substringToIndex: MIN(10, [productCSV.billingPeriodEnd length])];
            
        }
        
    }
}


- (void)parser:(CHCSVParser *)parser didReadComment:(NSString *)comment{
    NSLog(@"Parser read comment");
}


- (void)parser:(CHCSVParser *)parser didFailWithError:(NSError *)error{
    NSLog(@"Parser Fail");
}

@end
