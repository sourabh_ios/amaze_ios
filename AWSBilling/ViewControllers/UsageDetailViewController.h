//
//  UsageDetailViewController.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 15/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsageDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate , UISearchBarDelegate>
{
    bool isFiltered;
    UITapGestureRecognizer * tap;
}

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property (nonatomic, strong) NSArray *productList;
@property (nonatomic, strong) NSMutableArray *filteredList;
@property (nonatomic, strong) NSString *totalAmount;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
