//
//  BucketListViewController.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 03/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import "BucketListViewController.h"
#import <AWSiOSSDK/S3/AmazonS3Client.h>
#import "AmazonClientManager.h"
#import "MBProgressHUD.h"
#import "CHCSVParser.h"
#import "Product.h"
#import "Bill.h"
#import "DashBoardViewController.h"
#import "Constants.h"
#import "JNKeyChain.h"

@interface BucketListViewController () <MBProgressHUDDelegate, CHCSVParserDelegate>
{
    MBProgressHUD *HUD;
    Product *product;
    Bill *monthBill;
    int nNetworkOps;
}
@end

@implementation BucketListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    //self.view.alpha = 0;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self.navigationController action:nil];
    self.navigationItem.leftBarButtonItem = backButton;
    [self.navigationItem setTitle:@"AMAZE"];

    self.dashBoardList = [[NSMutableArray alloc] init];
    self.linkAccounts = [[NSMutableArray alloc] init];
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.delegate = self;
    [self.view addSubview:HUD];
    [self.bucketTableView setSeparatorColor:[self colorFromHexString:@"#f15a29"]];
    self.navigationController.navigationBar.barTintColor = [self colorFromHexString:@"#f15a29"];
    self.bucketTableView.contentInset = UIEdgeInsetsMake(-42, 0, 0, 0);
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* appStatus =    [defaults objectForKey:kAppStatus];
    if ([appStatus isEqualToString:@"BucketSaved"]) {
        [self performSegueWithIdentifier:@"pushDashBoardSegue" sender:self];
    }
    else
        [NSTimer scheduledTimerWithTimeInterval:0.0 target:self selector:@selector(getBucketLists) userInfo:nil repeats:NO];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

-(void)getBucketLists {
    HUD.labelText = @"Fetching Valid Buckets...";
    [HUD show:YES];

    if (bucketNames == nil) {
        bucketNames = [[NSMutableArray alloc] initWithCapacity:[self.bucketList count]];
    }
    else {
        [bucketNames removeAllObjects];
    }
    
    //Below program sort the buckets with creation date
    NSMutableArray *dateArray = [[NSMutableArray alloc] initWithCapacity:[self.bucketList count]];
    NSArray *reverseOrder;
    if (self.bucketList != nil) {
        for (S3Bucket *bucket in self.bucketList) {
            NSString *dateString = [[bucket creationDate] stringByReplacingOccurrencesOfString:@"T" withString:@" "];
            dateString = [dateString substringToIndex:[dateString length] -5];
            NSDateFormatter *datFormatter = [[NSDateFormatter alloc] init];
            [datFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate* date = [datFormatter dateFromString:dateString];
            [dateArray addObject:date];
            [bucketNames addObject:[bucket name]];
        }
        [dateArray sortedArrayUsingSelector:@selector(compare:)];
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"self"
                                                                   ascending:NO];
        NSArray *descriptors = [NSArray arrayWithObject:descriptor];
        reverseOrder = [dateArray sortedArrayUsingDescriptors:descriptors];
        
        [dateArray removeAllObjects];
        for (NSDate *date in reverseOrder) {
            NSDateFormatter *datFormatter = [[NSDateFormatter alloc] init];
            [datFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *string = [datFormatter stringFromDate:date];
            [dateArray addObject:string];
        }
        for (S3Bucket *bucket in self.bucketList) {
            NSString *dateString = [[bucket creationDate] stringByReplacingOccurrencesOfString:@"T" withString:@" "];
            dateString = [dateString substringToIndex:[dateString length] -5];
            NSInteger index = [dateArray indexOfObject:dateString];
            [bucketNames replaceObjectAtIndex:index withObject:[bucket name]];
        }
    }
    //Sorting Ends here
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    
        NSDate *date = [NSDate date];
        NSDateFormatter *datFormatter = [[NSDateFormatter alloc] init];
        [datFormatter setDateFormat:@"yyyy-MM"];
        NSString *dateString = [datFormatter stringFromDate:date];
        [self startNetworkingOperation];
        for (int i = 0; i < bucketNames.count; i++) {
            @try {
                stayPage = FALSE;
                S3ListObjectsRequest  *listObjectRequest = [[S3ListObjectsRequest alloc] initWithName:[bucketNames objectAtIndex:i]];
                S3ListObjectsResponse *listObjectResponse = [[AmazonClientManager s3] listObjects:listObjectRequest];
                S3ListObjectsResult   *listObjectsResults = listObjectResponse.listObjectsResult;
                
                for (S3ObjectSummary *objectSummary in listObjectsResults.objectSummaries) {
                    NSString *fileExtension = [objectSummary key];
                    NSLog(@"File Name: %@", fileExtension);
                    NSString *subString = [NSString stringWithFormat:@"-aws-billing-csv-%@",dateString];
                    if ([fileExtension containsString:subString]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [HUD setLabelText:@"Fetching Bills..."];
                            [self anOpHasFinished];
                        });
                        [self fetchBillsForBucket:[bucketNames objectAtIndex:i] andS3Result:listObjectsResults];
                        NSTimeInterval  today = [[NSDate date] timeIntervalSince1970];
                        NSString *intervalString = [NSString stringWithFormat:@"%.0f", today];
                        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setObject:intervalString forKey:kLastUpdated];
                        
                        bucketFound = TRUE;
                        goto BAIL;
                    }
                }

            }
            @catch (AmazonClientException *exception) {
                [HUD hide:YES];
                [self anOpHasFinished];
                stayPage = TRUE;
                __weak BucketListViewController *weakSelf = self;
                NSString *errorMessage;
                if ([exception.message containsString:@"The request timed out."])
                    errorMessage = @"Request timed out.";
                else if ([exception.message containsString:@"The Internet connection appears to be offline"])
                    errorMessage = @"The Internet connection appears to be offline";
                else if ([exception.message containsString:@"The network connection was lost"])
                    errorMessage = @"The network connection was lost";
                else
                    errorMessage = exception.message;
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Please try again"
                                                                                         message:errorMessage
                                                                                  preferredStyle:UIAlertControllerStyleActionSheet];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Try Again"
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction *action) {
                                                                         BucketListViewController *strongSelf = weakSelf;
                                                                         [strongSelf getBucketLists];
                                                                     }];
                [alertController addAction:cancelAction];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
    BAIL:
        if (!bucketFound) {
            NSLog(@"NO File Found");
            dispatch_async(dispatch_get_main_queue(), ^{
                if (stayPage == FALSE)
                    [self performSegueWithIdentifier:@"pushBucketToConfigureSegue" sender:self];
            });
        }
    });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return [bucketNames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = @"";//[bucketNames objectAtIndex:indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void) fetchBillsForBucket : (NSString *)bucket andS3Result:(S3ListObjectsResult *)s3Result {
    S3GetObjectRequest  *getObjectRequest;
    S3GetObjectResponse *getObjectResponse;
    HUD.labelText = @"Fetching Bills..";
    self.dashBoardList = [[NSMutableArray alloc] init];
    @try {
        
        [self startNetworkingOperation];
        if (objects == nil) {
            objects = [[NSMutableArray alloc] initWithCapacity:[s3Result.objectSummaries count]];
        }
        else {
            [objects removeAllObjects];
        }
        for (S3ObjectSummary *objectSummary in s3Result.objectSummaries) {
            
            if ([[objectSummary key] rangeOfString:@"-aws-billing-csv-"].location != NSNotFound)
                [objects addObject:[objectSummary key]];
        }
        [objects sortUsingSelector:@selector(compare:)];
        
        
        if (objects.count == 0) {
            UIAlertView *anAlert = [[UIAlertView alloc] initWithTitle:@"No Billing Data" message:@"Please Select valid bucket and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [anAlert show];
        }
        else {
            for (int monthCount = objects.count; monthCount > 0; monthCount--) {
                getObjectRequest = [[S3GetObjectRequest alloc] initWithKey:[objects objectAtIndex:monthCount - 1] withBucket:bucket];
                getObjectResponse = [[AmazonClientManager s3] getObject:getObjectRequest];
                NSString *subString = [[objects objectAtIndex:monthCount - 1]  stringByReplacingOccurrencesOfString:@".csv" withString:@""];
                subString = [subString substringFromIndex:[subString length]-7];
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *fileExten = [NSString stringWithFormat:@"%@.csv",subString];
                NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileExten];
                
                [getObjectResponse.body writeToFile:filePath atomically:YES];

                if (monthCount == objects.count) {
                    [self performSegueWithIdentifier:@"pushDashBoardSegue" sender:self];
                }
            }
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@"BucketSaved" forKey:kAppStatus];
            reverseArray = TRUE;
            [defaults setObject:bucket forKey:kBucketName];
            [defaults synchronize];
            
            //Notification
            [self anOpHasFinished];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDate *date = [NSDate date];
                NSDateFormatter *datFormatter = [[NSDateFormatter alloc] init];
                [datFormatter setDateFormat:@"yyyy-MM"];
                 NSString *selectedMonth = [datFormatter stringFromDate:date];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshPreviousMonths" object:selectedMonth];
            });
        }
    }
    
        @catch (AmazonClientException *exception) {
            [HUD hide:YES];
            [self anOpHasFinished];
            __weak BucketListViewController *weakSelf = self;
            NSString *errorMessage;
            if ([exception.message containsString:@"The request timed out."])
                errorMessage = @"Request timed out.";
            else if ([exception.message containsString:@"The Internet connection appears to be offline"])
                errorMessage = @"The Internet connection appears to be offline";
            else if ([exception.message containsString:@"The network connection was lost"])
                errorMessage = @"The network connection was lost";
            else
                errorMessage = exception.message;
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Please try again"
                                                                                     message:errorMessage
                                                                              preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Try Again"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction *action) {
                                                                     BucketListViewController *strongSelf = weakSelf;
                                                                     [strongSelf getBucketLists];
                                                                 }];
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    
    [HUD hide:YES];
}

/*
- (void)csvParseWithString : (NSData *)csvData {
    CHCSVParser *csvParser = [[CHCSVParser alloc] initWithCSVString:[[NSString alloc] initWithData:csvData encoding:NSUTF8StringEncoding]];
    csvParser.delegate = self;
    [csvParser parse];
}


- (void)parserDidBeginDocument:(CHCSVParser *)parser{
    
    productList = [[NSMutableArray alloc] init];
    endOfProductList = FALSE;
    billParseEndState = 0;
}


- (void)parserDidEndDocument:(CHCSVParser *)parser{
    for (Product *tempProduct in productList) {
        if (![self.linkAccounts containsObject:tempProduct.linkedAccount]) {
            [self.linkAccounts addObject:tempProduct.linkedAccount];
        }
    }
    monthBill.monthDetails = productList;
    [self.dashBoardList addObject:monthBill];
}


- (void)parser:(CHCSVParser *)parser didBeginLine:(NSUInteger)recordNumber{
    parserLine = recordNumber;
    product = [[Product alloc] init];
}


- (void)parser:(CHCSVParser *)parser didEndLine:(NSUInteger)recordNumber{
    if (parserLine > 1 && !endOfProductList == TRUE) {
        [productList addObject:product];
    }
    else if (endOfProductList == TRUE)
        billParseEndState++;
}


- (void)parser:(CHCSVParser *)parser didReadField:(NSString *)field atIndex:(NSInteger)fieldIndex{
    field = [field stringByReplacingOccurrencesOfString:@"\"" withString:@""];

    if (fieldIndex == 3 && ([field isEqualToString:@"CBRounding"] || [field isEqualToString:@"InvoiceTotal"])) {
        endOfProductList = TRUE;
       if ([field isEqualToString:@"CBRounding"])
            billParseEndState = 0;
       else if ([field isEqualToString:@"InvoiceTotal"])
            billParseEndState = 1;
       
    }
    else if (endOfProductList == TRUE) {
        if (billParseEndState == 1) {
            if (fieldIndex == 23)
                product.currency = field;
            else if (fieldIndex == 28)
                product.productAmount = field;
            else if (fieldIndex == 1)
                payerAccountID = field;
            else if (fieldIndex == 8)
                payerName = field;
            
            monthBill.monthTotal = [product.productAmount stringByAppendingString:product.currency];
            monthBill.monthTotal = [monthBill.monthTotal stringByReplacingOccurrencesOfString:@"\"" withString:@" "];
        }
        
        
    }
    else {
        if (parserLine > 1 && fieldIndex == 13) {
            if ([field isEqualToString:@"Amazon Cognito"]) {
                product.abbrevatedName = @"Cognito";
                product.imageName = @"SimpleIcon_Cognito.png";
            }
            else if ([field isEqualToString:@"Amazon Mobile Analytics"]) {
                product.abbrevatedName = @"Mobile Analytics";
                product.imageName = @"MobileAnalytics.png";
            }
            else if ([field isEqualToString:@"AWS Data Transfer"]) {
                product.abbrevatedName = @"Data Transfer";
                product.imageName = @"AWS.png";
            }
            else if ([field isEqualToString:@"Amazon Simple Notification Service"]) {
                product.abbrevatedName = @"SNS";
                product.imageName = @"SNS.png";
            }
            else if ([field isEqualToString:@"Amazon Simple Storage Service"]) {
                product.abbrevatedName = @"S3";
                product.imageName = @"S3.png";
            }
            else if ([field isEqualToString:@"Amazon Elastic File System"]) {
                product.abbrevatedName = @"EFS";
                product.imageName = @"efs.png";
            }
            else if ([field isEqualToString:@"AWS Storage Gateway"]) {
                product.abbrevatedName = @"Storage Gateway";
                product.imageName = @"StorageGateway.png";
            }
            else if ([field isEqualToString:@"Amazon Glacier"])
                product.abbrevatedName = @"Glacier";
            else if ([field isEqualToString:@"Amazon CloudFront"]) {
                product.abbrevatedName = @"CloudFront";
                product.imageName = @"cloudfront.png";
            }
            else if ([field isEqualToString:@"Amazon Elastic Block Store"]) {
                product.abbrevatedName = @"EBS";
                product.imageName = @"aws-ebs.png";
            }
            else if ([field isEqualToString:@"AWS Directory Service"])
                product.abbrevatedName = @"Directory Services";
            else if ([field isEqualToString:@"Amazon Identity and Access Management"]) {
                product.abbrevatedName = @"IAM";
                product.imageName = @"iam.png";
            }
            else if ([field isEqualToString:@"AWS CloudTrail"])
                product.abbrevatedName = @"CloudTrail";
            else if ([field isEqualToString:@"AWS Config"])
                product.abbrevatedName = @"Config";
            else if ([field isEqualToString:@"Amazon CloudWatch"]) {
                product.abbrevatedName = @"CloudWatch";
                product.imageName = @"cloudwatch.png";
            }
            else if ([field isEqualToString:@"AWS Key Management Service"])
                product.abbrevatedName = @"KMS";
            else if ([field isEqualToString:@"AWS CloudHSM"])
                product.abbrevatedName = @"CloudHSM";
            else if ([field isEqualToString:@"Amazon Elastic Compute Cloud"]) {
                product.abbrevatedName = @"EC2";
                product.imageName = @"aws-ec2.png";
            }
            else if ([field isEqualToString:@"Amazon EC2 Container Service"])
                product.abbrevatedName = @"EC2 Container";
            else if ([field isEqualToString:@"Auto Scaling"])
                product.abbrevatedName = @"Auto Scaling";
            else if ([field isEqualToString:@"Elastic Load Balancing"])
                product.abbrevatedName = @"Load Balancing";
            else if ([field isEqualToString:@"AWS Lambda"])
                product.abbrevatedName = @"Lambda";
            else if ([field isEqualToString:@"Amazon Virtual Private Cloud"])
                product.abbrevatedName = @"VPC";
            else if ([field isEqualToString:@"Amazon Route 53"]) {
                product.abbrevatedName = @"Route 53";
                product.imageName = @"aws-route-53.png";
            }
            else if ([field isEqualToString:@"Amazon Simple Queue Service"]) {
                product.abbrevatedName = @"SQS";
                product.imageName = @"aws-sqs.png";
            }
            else if ([field isEqualToString:@"AWS Direct Connect"])
                product.abbrevatedName = @"Direct Connect";
            else if ([field isEqualToString:@"Amazon WorkSpaces"])
                product.abbrevatedName = @"WorkSpaces";
            else if ([field isEqualToString:@"Amazon WorkSpaces Application Manager"])
                product.abbrevatedName = @"WAM";
            else if ([field isEqualToString:@"Amazon WorkDocs"])
                product.abbrevatedName = @"WorkDocs";
            else if ([field isEqualToString:@"Amazon WorkMail (preview)"])
                product.abbrevatedName = @"WorkMail";
            else if ([field isEqualToString:@"Amazon Elastic MapReduce"]) {
                product.abbrevatedName = @"EMR";
                product.imageName = @"aws-emr.png";
            }
            else if ([field isEqualToString:@"Amazon Kinesis"])
                product.abbrevatedName = @"Kinesis";
            else if ([field isEqualToString:@"AWS Data Pipeline"])
                product.abbrevatedName = @"Data Pipeline";
            else if ([field isEqualToString:@"Amazon Machine Learning"])
                product.abbrevatedName = @"Machine Learning";
            else
                product.abbrevatedName = field;
            
            if (product.imageName == nil) {
                product.imageName = @"AWS.png";
            }
            product.productName = field;
        }
        else if (parserLine > 1 && fieldIndex == 15)
            product.usageType = field;
        else if (parserLine > 1 && fieldIndex == 18)
            product.usageDescription = field;
        else if (parserLine > 1 && fieldIndex == 23)
            product.currency = field;
        else if (parserLine > 1 && fieldIndex == 21)
            product.productQuantity = field;
        
        else if (parserLine > 1 && fieldIndex == 28) {
            product.productAmount = field;
            product.productPrice = [product.productAmount floatValue];
        }
        else if (parserLine > 1 && fieldIndex == 9)
            product.linkedAccount = field;
        else if (parserLine > 1 && fieldIndex == 2)
            product.linkID = field;
        else if (parserLine > 1 && fieldIndex == 7) {
            product.invoiceDate = field;
            product.invoiceDate = [product.invoiceDate substringToIndex: MIN(10, [product.invoiceDate length])];
        }
        else if (parserLine > 1 && fieldIndex == 8)
            product.productPayer = field;
        else if (parserLine > 1 && fieldIndex == 10)
            product.productAddress = field;
        else if (parserLine > 1 && fieldIndex == 14)
            product.productSeller = field;
        else if (parserLine > 1 && fieldIndex == 5) {
            product.billingPeriodStart = field;
            product.billingPeriodStart = [product.billingPeriodStart substringToIndex: MIN(10, [product.billingPeriodStart length])];
        }
        else if (parserLine > 1 && fieldIndex == 6) {
            product.billingPeriodEnd = field;
            product.billingPeriodEnd = [product.billingPeriodEnd substringToIndex: MIN(10, [product.billingPeriodEnd length])];

        }
    }
}


- (void)parser:(CHCSVParser *)parser didReadComment:(NSString *)comment{
     NSLog(@"Parser read comment");
}


- (void)parser:(CHCSVParser *)parser didFailWithError:(NSError *)error{
     NSLog(@"Parser Fail");
}
*/
/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"pushDashBoardSegue"])
    {
//        DashBoardViewController *dashBoardVC = [segue destinationViewController];
//        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//        NSString* appStatus =    [defaults objectForKey:kAppStatus];
//        if ([appStatus isEqualToString:@"BucketSaved"] && reverseArray) {
//            self.dashBoardList = [[self.dashBoardList reverseObjectEnumerator] allObjects];
//        }
//        dashBoardVC.csvData = self.dashBoardList;
//        dashBoardVC.payerID = payerAccountID;
//        dashBoardVC.payerName = payerName;
//        dashBoardVC.linkAccounts = self.linkAccounts;
//        dashBoardVC.autoRefresh = TRUE;
    }
}

*/
- (void)startNetworkingOperation
{
    nNetworkOps++;
    NSLog(@"Network Indicator Count: %d", nNetworkOps);
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

}

- (void)anOpHasFinished
{
    nNetworkOps--;
    NSLog(@"Network Indicator Count: %d", nNetworkOps);
    if (nNetworkOps == 0) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}

@end
