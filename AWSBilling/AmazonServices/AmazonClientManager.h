//
//  AmazonClientManager.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 03/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AWSiOSSDK/S3/AmazonS3Client.h>

@interface AmazonClientManager : NSObject

+(AmazonS3Client *)s3;
+(void)validateCredentials;
@end
