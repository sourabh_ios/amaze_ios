//
//  AmazonClientManager.m
//  AWSBilling
//
//  Created by Sourabh Kumar on 03/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import "AmazonClientManager.h"
#import <AWSiOSSDK/AmazonLogger.h>
#import "JNKeychain.h"
#import "Constants.h"

static AmazonS3Client       *s3  = nil;

@implementation AmazonClientManager

+(AmazonS3Client *)s3
{
    [AmazonClientManager validateCredentials];
    return s3;
}

+(void)validateCredentials
{
        NSString *accessKey = [JNKeychain loadValueForKey:kAccessKey];
        NSString *secretKey = [JNKeychain loadValueForKey:kSecretKey];
        s3 = [[AmazonS3Client alloc] initWithAccessKey:accessKey withSecretKey:secretKey];
}

@end
