//
//  Constants.h
//  AWSBilling
//
//  Created by Sourabh Kumar on 03/06/15.
//  Copyright (c) 2015 TechChefs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject
#define kAccessKey   @"accessKey"
#define kSecretKey   @"secretKey"
#define kAppStatus   @"appStatus"
#define kBucketName   @"bucketName"
#define kLastUpdated @"lastUpdated"

@end
